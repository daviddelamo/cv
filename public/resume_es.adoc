= Curriculum Vitae - David del Amo Mateos

David del Amo Mateos - david.delamo@gmail.com - 24/03/2019

== Desarrollador de software

Como desarrollador de software me apasiona investigar nuevas tecnologías. Aprender distintos paradigmas, lenguajes y formas de trabajar y aplicarlos a mi día a día. Actualmente estoy particularmente interesado en el modelo reactivo y los lenguajes funcionales después de haber trabajado con Reactor en los últimos meses.


== Experiencia

=== UST-Global, Banco Santander - Arquitecto de software
Marzo 2018 - Enero 2019
Evolución de la arquitectura de referencia de microservicios del Banco Santander migrando de Spring Boot 1.5 a Spring Boot 2.1. 
Dentro de las labores de evolución del software se realizó la migración de los componentes de trazas y seguridad al modelo reactivo, empleando para ello las librerias de Reactor, de cara a poder ser usados en cualquier aplicación desarrollada dentro de la organización que quisiera emplear dicho modelo. 
Migración de la documentación de la arquitectura para emplear AsciiDoc en lugar de confluence.
Migración de la librería Hystrix a Resilience4j. Para realizar la migración se estudiaron las diferencias entre ambas librerias y se opto inicialmente por crear una anotación propia que permitiese abstraer el código de la librería seleccionada.

=== UST-Global, Banco Santander - Arquitecto de software
Septiembre 2017 - Marzo 2018
Definición de la nueva arquitectura de referencia de microservicios del Banco Santander.
Desarrollo de componentes de arquitectura entre los que se incluyen, generación de logs a medida diseñados para poder ser digeridos por logstash, autenticación de usuarios basado en tokens JWT, gestión de mensajería a través de kafka y spring-cloud-stream con soporte para la infraestructura específica del cliente, diseño de un motor de transaccionalidad basado en el patrón SAGA y creación de un arquetipo maven para facilitar el desarrollo de microservicios.
Definición de buenas prácticas de desarrollo orientado a microservicios, como por ejemplo el empleo del patrón circuit breaker  usando Hystrix, CQRS para el almacenamiento y consulta de datos y cacheo de datos empleado spring-cache.
Tecnologías empleadas: Spring Boot 1.5, Spring-Cloud-Stream, Apache Kafka, Log4j2, Docker, Openshift, Hystrix, MongoDb, Redis.

=== Atos,  Correos - Arquitecto de software
Mayo 2015 - Septiembre 2017
Soporte al desarrollo y mantenimiento de las aplicaciones de Admisión en Correos usando Struts, Spring 4, EJB3, Hibernate, EclipseLink, JPA, Maven, Quartz y Spring Batch.

=== Atos, SEUR- Analista - Programador
Noviembre 2014 - Mayo 2015
Mantenimiento y desarrollo de aplicativo ALEN en SEUR encargado de gestionar las lecturas realizadas en los almacenes de SEUR. Desarrollado en Java con Struts, Java WebStart, Swing y usando la máquina virtual CreMe en las terminales.

=== Atos, Grupo ZENA- Analista - Programador
Junio 2013 - Noviembre 2014
Mantenimiento y desarrollo de evolutivos del operacional de Domino's Pizza realizado en Java con Struts, jquery e iBatis.

=== Atos, Orange- Analista
Enero 2013 - Junio 2013
Analisis de nueva aplicación para gestionar las migraciones y cambios de tarifa de Orange.

=== Atos, Correos - Programador
Marzo 2012 - Enero 2013
Desarrollo sistema EUROGIRO para Correos usando JSF con Trinidad e hibernate.

=== Atos, SEUR - Analista - Programador
Marzo 2010 - Marzo 2012
Desarrollo del sistema operacional de SEUR usando SEAM, Richfaces, EJB3.0 e Hibernate.

=== Atos, SEUR - Analista - Programador
Marzo 2009 - Marzo 2010
Actividad:    Desarrollo de un prototipo del Nuevo Sistema Operacional de SEUR usando GWT, EJB3.0 y JDBC.
Funciones: Diseño técnico del sistema, formación a los desarrolladores, soporte a certificación, gestión de la configuración.

=== Atos, ICM - Analista - Programador
Marzo 2008 - Febrero 2009
Actividad:    Desarrollo de aplicaciones para ICM. Uso del framework propio de ICM con Java, JSP's y JDBC.
Funciones:    Formación a los desarrolladores, gestión de la configuración, gestión de un equipo de 3 personas.
Grupo Delaware, ICM - Analista - Programador
Septiembre 2005 - Marzo 2008
Actividad:    Mantenimiento del framework propio de ICM con Java, JSP's y JDBC. Desarrollo de soluciones para las distintas arquitecturas de ICM.
Funciones:    Realizo una labor de soporte técnico en el Organismo Autónomo de Informática y Comunicaciones de la Comunidad de Madrid. En este puesto ayudo a otros programadores a desarrollar aplicaciones Java y desarrollo el framework Java que se usa en ICM.
Desarrollo de sistema de firma electrónica, apoyo al desarrollo de la solución para el envío de mensajes seguros entre WebServices firmados digitalmente.

=== Bilbomática, ICM - Programador
Septiembre 2001 - Septiembre 2005
Actividad:    Desarrollo del framework propio de ICM con Java, JSP's y JDBC. Desarrollo de soluciones para las distintas arquitecturas de ICM.
Funciones:    Diseño y desarrollo de framework de desarrollo de aplicaciones J2EE para ICM.
Desarrollo de aplicaciones basadas en esquemas XML usando JAXB
Desarrollo de aplicaciones usando Servlets y plantillas HTML.
Desarrollo de aplicaciones seguras empleando SSL v3 con certificados de cliente y firma digital.
Desarrollo de webservices para interoperabilidad entre Delphi y Java
Desarrollo de componentes para realizar firmas digitales de ficheros y validaciones de las mismas.
Mantenimiento aplicación TED.
Mantenimiento arquitectura de desarrollo de ICM basada en servidores OC4J y Apache en máquinas UNIX.
Migración de CGI’s desarrollados en Delphi al framework Java de ICM


== Educación

=== Udemy, Online - Desarrollo de aplicaciones con Angular 4
ENERO 2017 - ENERO 2017
Curso online de desarrollo de aplicaciones con Angular 4, MongoDB y Express.

=== Certified Scrum Developer, Estratecno  
SEPTIEMBRE 2016 - SEPTIEMBRE 2016
Curso para la certificación de Scrum Developer impartido en Estratecno.

=== Instituto Cibernos, Madrid- Curso de desarrollo de aplicaciones informáticas
SEPTIEMBRE 1999 - MARZO 2002
Curso sobre desarrollo de aplicaciones informáticas en distintos lenguajes, entre los que se incluye SQL / PLSQL, Java, Visual Basic, C/C++.
